<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>    
    <style>
        .ok {
            display: inline-block;
            margin-top: 10px;
            padding: 5px;
            color: #222;
            background-color: #34e834;
        }
        .error {
            display: inline-block;
            margin-top: 10px;
            padding: 5px;
            color: #fff;
            background-color: red;
        }
    </style>
</head>
<body>

<br>
<br>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis iste perferendis quod dolorum aperiam tempora vero consequatur ad autem quibusdam eius, consequuntur enim recusandae minima cupiditate molestias hic quisquam blanditiis.</p>
<br>
<br>

<form id="contacto" action="<?php echo htmlspecialchars( $_SERVER[ "PHP_SELF" ] ); ?>" method="post">
    <label for="c_nombre">Nombre:</label><br>
    <input type="text" name="nombre" id="c_nombre" required>
    <br><br>
    <label for="c_email">Email:</label><br>
    <input type="email" name="email" id="c_email">
    <br><br>
    <label for="c_mensaje">Mensaje:</label><br>
    <textarea name="mensaje" id="c_mensaje" cols="30" rows="10"></textarea>
    <br><br>
    <label>Color:</label><br>
    <label for="c_rojo">Rojo</label>
    <input type="radio" name="colors" id="c_rojo" value="rojo" required>
    <label for="c_verde">Verde</label>
    <input type="radio" name="colors" id="c_verde" value="verde">
    <label for="c_azul">Azul</label>
    <input type="radio" name="colors" id="c_azul" value="azul">
    <br><br>
    <label>Selecciona un día</label>
    <select name="semana">
        <option value="l">Lunes</option>
        <option value="m">Martes</option>
        <option value="x">Miércoles</option>
        <option value="j">Jueves</option>
        <option value="v">Viernes</option>
    </select>
    <br><br>
    <label for="c_legal">He leído y acepto la nota legal</label>
    <input type="checkbox" name="legal" id="c_legal" value="legal">
    <br>
    <label for="c_publi">Acepto recibir publicidad</label>
    <input type="checkbox" name="publi" id="c_publi" value="publi">
    <br><br><br>
    <input type="hidden" name="origen" value="fomulario_contacto">
    <input type="submit" value="Enviar">    
</form>

<?php

if( ($_SERVER["REQUEST_METHOD"] == "POST") && isset($_POST) ) {

    /**
     * recuperar datos
     */

    $nombre = $email = $mensaje = $color = $dia = $legal = $publi = $origen = '';


    /**
     * sanear datos
     */
    function sanear_datos( $data ) {
        $data = trim( $data );
        $data = stripslashes( $data );
        $data = htmlspecialchars( $data );
        return $data;
    }

    if ( isset( $_POST['nombre']) ) $nombre = sanear_datos( $_POST['nombre'] );
    if ( isset( $_POST['email']) ) $email = sanear_datos( $_POST['email'] );
    if ( isset( $_POST['mensaje']) ) $mensaje = sanear_datos( $_POST['mensaje'] );
    if ( isset( $_POST['colors']) ) $color = sanear_datos( $_POST['colors'] );
    if ( isset( $_POST['semana']) ) $dia = sanear_datos( $_POST['semana'] );
    if ( isset( $_POST['legal']) ) $legal = sanear_datos( $_POST['legal'] );
    if ( isset( $_POST['publi']) ) $publi = sanear_datos( $_POST['publi'] );
    if ( isset( $_POST['origen']) ) $origen = sanear_datos( $_POST['origen'] );

        
    /**
     * envío de formulario
     */

    $to = 'aleix.marti@interactius.com';
    
    $subject = 'Formulario contacto desde web';
   
    $msg = 'nombre: ' . $nombre . "\r\n";
    $msg .= 'email: ' . $email . "\r\n";
    $msg .= 'mensaje: ' . $mensaje . "\r\n";
    $msg .= 'color: ' . $color . "\r\n";
    $msg .= 'dia: ' . $dia . "\r\n";
    $msg .= 'legal: ' . $legal . "\r\n";
    $msg .= 'publi: ' . $publi . "\r\n";
    $msg .= 'origen: ' . $origen . "\r\n";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: <webmaster@example.com>' . "\r\n";
    $headers .= 'Cc: myboss@example.com' . "\r\n";

    if( mail( $to, $subject, $msg, $headers ) ) {
        echo "<div class='ok'>Email enviado con éxito</div>";
    }else{
        echo "<div class='error'>Error al enviar el email. Vuelve a intentarlo más tarde</div>";
    }                

} ?>

<br>
<br>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis iste perferendis quod dolorum aperiam tempora vero consequatur ad autem quibusdam eius, consequuntur enim recusandae minima cupiditate molestias hic quisquam blanditiis.</p>
<br>
<br>

</body>

</html>
