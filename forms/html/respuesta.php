

<?php

if( ($_SERVER["REQUEST_METHOD"] == "POST") && isset($_POST) ) {

    /**
     * recuperar datos
     */

    $nombre = $email = $mensaje = $color = $dia = $legal = $publi = $origen = '';


    /**
     * sanear datos
     */
    function sanear_datos( $data ) {
        $data = trim( $data );
        $data = stripslashes( $data );
        $data = htmlspecialchars( $data );
        return $data;
    }

    if ( isset( $_POST['nombre']) ) $nombre = sanear_datos( $_POST['nombre'] );
    if ( isset( $_POST['email']) ) $email = sanear_datos( $_POST['email'] );
    if ( isset( $_POST['mensaje']) ) $mensaje = sanear_datos( $_POST['mensaje'] );
    if ( isset( $_POST['colors']) ) $color = sanear_datos( $_POST['colors'] );
    if ( isset( $_POST['semana']) ) $dia = sanear_datos( $_POST['semana'] );
    if ( isset( $_POST['legal']) ) $legal = sanear_datos( $_POST['legal'] );
    if ( isset( $_POST['publi']) ) $publi = sanear_datos( $_POST['publi'] );
    if ( isset( $_POST['origen']) ) $origen = sanear_datos( $_POST['origen'] );

        
    /**
     * envío de formulario
     */

    $to = 'aleix.marti@interactius.com';
    
    $subject = 'Formulario contacto desde web';
   
    $msg = 'nombre: ' . $nombre . "\r\n";
    $msg .= 'email: ' . $email . "\r\n";
    $msg .= 'mensaje: ' . $mensaje . "\r\n";
    $msg .= 'color: ' . $color . "\r\n";
    $msg .= 'dia: ' . $dia . "\r\n";
    $msg .= 'legal: ' . $legal . "\r\n";
    $msg .= 'publi: ' . $publi . "\r\n";
    $msg .= 'origen: ' . $origen . "\r\n";

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: <webmaster@example.com>' . "\r\n";
    $headers .= 'Cc: myboss@example.com' . "\r\n";

    if( mail( $to, $subject, $msg, $headers ) ) {
        echo "Email enviado con éxito";
    }else{
        echo "Error al enviar el email. Vuelve a intentarlo más tarde";
    }

    
            

} else {
    echo "Error en el envío del formulario";
}

?> 
<br>
<a href="http://daw.test/daw-2018/forms/">Volver</a>