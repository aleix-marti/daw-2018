$(document).ready(function () {
    
    // recibe JSON como respuesta
    // cambia el estilo del mensaje de respuesta
    $("#contacto").submit(function (e) {

        e.preventDefault();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            dataType: 'JSON',
            success: function (data) {
                console.dir(data);
                console.log(data.msg);
                console.log(data.type);
                $('#respuesta').text(data.msg);
                $('#respuesta').addClass(data.type);
            }
        });
    });

});
