$(document).ready(function () {
    
    //muestra un mensaje de respuesta
    $("#contacto").submit( function(e){

        e.preventDefault();

        var form = $(this);
        var url = form.attr('action');

        $.ajax ({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data) {
                console.dir(data);
                $('#respuesta').text(data);                
            }
        });        
    });

});
